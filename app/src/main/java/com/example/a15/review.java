package com.example.a15;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.EventLog.Event;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.FirebaseRecyclerOptions.Builder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class review extends AppCompatActivity {
    private Button mBtn_submit_review;
    private RatingBar rb;

    private RecyclerView mFeedbackList;
    private DatabaseReference inviteRef;
    private FirebaseAuth mAuth;
    private FirebaseRecyclerAdapter<review_firebase, review.reviewViewHolder> adapter;
    private SimpleDateFormat mdformat;
    private String today_date;
    private String UID;
    private List<String> all_members = new ArrayList<String>();
    private DatabaseReference DB_ref = null;
    private boolean member = false;
    private String key;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_list);

        mAuth = FirebaseAuth.getInstance();
        UID = mAuth.getUid();
        mdformat = new SimpleDateFormat("d M yyyy");
        today_date = mdformat.format(Calendar.getInstance().getTime());
        Log.d("logcat_date", today_date);
        //inviteRef = FirebaseDatabase.getInstance().getReference().child("Invitation").child(today_date + "_" + UID).child("Invitation").child("Members");
        inviteRef = FirebaseDatabase.getInstance().getReference().child("Invitation");

        inviteRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
/*
                    Log.d("logcat", snapshot.getKey());
                    if (snapshot.child("Date").getValue().toString().matches(today_date)){
                        all_members.add(snapshot.child("Initiator").getValue().toString());
                        Log.d("logcat", String.valueOf(all_members.size()));

                        for (DataSnapshot snapshot1: snapshot.child("Members").getChildren()){
                            all_members.add(snapshot1.getValue().toString());
                            Log.d("size", String.valueOf(all_members.size()));
                        }

 */
                    Log.d("logcat", snapshot.child("Date").getValue().toString());
                    if (snapshot.child("Date").getValue().toString().matches(today_date)) {
                        Log.d("logcat_date_match", snapshot.getKey());
                        key = snapshot.getKey();
                        if (snapshot.child("Initiator").getValue().toString().matches(UID)) {
                            //Record if "Members" exist
                            member = true;

                            //key = snapshot.getKey();
                            //DB_ref = FirebaseDatabase.getInstance().getReference().child("Invitation").child(key);
                            Log.d("logcat_key", key);
                        } else if (snapshot.child("Members").exists()) {
                            //Record if "Members" exist
                            member = true;

                            for (DataSnapshot snapshot1 : snapshot.child("Members").getChildren()) {
                                if (snapshot1.getValue().toString().matches(UID)) {
                                    //key = snapshot.getKey();
                                    Log.d("logcat_key", key);
                                    DB_ref = FirebaseDatabase.getInstance().getReference().child("Invitation").child(key);
                                }
                            }
                        }
                    }

                    }
                if (member == true) {
                    DisplayAllreview();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

/*
        inviteRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d("Child", dataSnapshot.getKey());
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.d("Child1", snapshot.getKey());
                    if (snapshot.child("Members").exists()) {
                        all_members.add(snapshot.child("Initiator").getValue().toString());
                        Log.d("all member", all_members.get(0));
                        for (DataSnapshot snapshot1 : snapshot.child("Members").getChildren()) {
                            Log.d("Child2", snapshot1.getKey());
                            all_members.add(snapshot1.getValue().toString());
                        }

                        if (all_members.contains(UID)){
                            all_members.remove(UID);
                            for (int k = 0; k < all_members.size(); k++) {
                                ReviewViewHolder.name.setText(all_members.get(k));
                                Log.d("Text", all_members.get(k));
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

 */

        mFeedbackList = (RecyclerView)findViewById(R.id.feedback_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setReverseLayout(true);
//        linearLayoutManager.setStackFromEnd(true);
        mFeedbackList.setLayoutManager(linearLayoutManager);

        mBtn_submit_review = (Button) findViewById(R.id.btn_submit_review);

/*
        rb =(RatingBar)findViewById(R.id.ratingBar_all);

        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                //rating可記錄星星顆數
            }
        });

 */



        mBtn_submit_review.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //把review 寫入database (待寫）

//                rb.getNumStars();
//                rb.getRating();

                //Toast message indicate that button being pressed
                Toast.makeText(getApplicationContext(),"評分已送出!",Toast.LENGTH_LONG).show();

                //評分完成, 結束activity
                finish();
            }
        });


    }

    private void DisplayAllreview() {
        FirebaseRecyclerOptions<review_firebase> options = new Builder<review_firebase>().setQuery(DB_ref, review_firebase.class).build();
        adapter
                = new FirebaseRecyclerAdapter<review_firebase, reviewViewHolder>(options) {

            @Override
            protected void onBindViewHolder(@NonNull final reviewViewHolder ReviewViewHolder, final int i, @NonNull review_firebase review_firebase) {
                String node = getRef(i).getKey();
                Log.d("TAG:  ", node);

                inviteRef.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("logcat_recyclerView", dataSnapshot.getKey());
//                        if (!dataSnapshot.child("Initiator").getValue().toString().matches(UID)){
                            all_members.add(dataSnapshot.child("Initiator").getValue().toString());
//                        }

                        for (DataSnapshot snapshot: dataSnapshot.child("Members").getChildren()){
                            all_members.add(snapshot.getValue().toString());
                        }
                        all_members.remove(UID);

                        ReviewViewHolder.name.setText(all_members.get(i));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @NonNull
            @Override
            public reviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                review.super.onStart();

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_users_feedback, parent, false);

                return new reviewViewHolder(view);
            }

        };
        adapter.startListening();
        mFeedbackList.setAdapter(adapter);
    }

    private class reviewViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        RatingBar rating;


        public reviewViewHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.name_feedback);
            rating = (RatingBar)itemView.findViewById(R.id.ratingBar_all);
        }

        public void setProfileImage(Context ctx, String profileImage){
            ImageView mimage = (ImageView)itemView.findViewById(R.id.image_feedback);
            Picasso.get().load(profileImage).into(mimage);
        }
    }
    @Override
    protected void onStop() {
        if (adapter != null)
            adapter.stopListening();
        super.onStop();
    }
}
