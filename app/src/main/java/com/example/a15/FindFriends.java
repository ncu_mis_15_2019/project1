package com.example.a15;

import android.content.Context;


public class FindFriends {
    private String ProfileImage;
    private String Name;
    private String Sex;
    private String Birthday;
    private String Basketball_LV;
    private String Volleyball_LV;
    private String Football_LV;
    private String Status;

    public FindFriends() {
    }  //Needs for Firebase


    public FindFriends(String profileImage, String name, String sex, String birthday, String basketball_LV, String volleyball_LV, String football_LV, String status) {
        ProfileImage = profileImage;
        Name = name;
        Sex = sex;
        Birthday = birthday;
        Basketball_LV = basketball_LV;
        Volleyball_LV = volleyball_LV;
        Football_LV = football_LV;
        Status = status;
    }

    public String getProfileImage(){
        return ProfileImage;
    }

    public void setProfileImage(Context ctx,String profileImage){
        ProfileImage = profileImage;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getBasketball_LV() {
        return Basketball_LV;
    }

    public void setBasketball_LV(String basketball_LV) {
        Basketball_LV = basketball_LV;
    }

    public String getVolleyball_LV() {
        return Volleyball_LV;
    }

    public void setVolleyball_LV(String volleyball_LV) {
        Volleyball_LV = volleyball_LV;
    }

    public String getFootball_LV() {
        return Football_LV;
    }

    public void setFootball_LV(String football_LV) {
        Football_LV = football_LV;
    }

    public String getStatus(){return Status;}

    public void setStatus(String statuss){Status = statuss;}
}
