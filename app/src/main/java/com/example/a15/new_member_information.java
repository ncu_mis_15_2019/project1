package com.example.a15;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

public class new_member_information extends AppCompatActivity {
    private Button mConfirmRegister;
    private Spinner mSpinner_sex;
    private Spinner mSpinner_basketball_level;
    private Spinner mSpinner_football_level;
    private Spinner mSpinner_vollyball_level;
    private CheckBox mCheckBox_Basketball;
    private CheckBox mCheckBox_Football;
    private CheckBox mCheckBox_Volleyball;
    private ImageButton mImageButton;
    private EditText mEditText_email;
    private EditText mEditText_phone;
    private TextInputLayout mEditText_password,mEditText_confirm;
    private EditText mEditText_name;
    private FirebaseAuth mAuth;
    private TextView theDate;
    private ImageView mImageButton_cal;
    private DatePickerDialog.OnDateSetListener mDateSetListner;
    private static final String TAG = "MainActivity";
    private String msLevel_basketball;
    private String msLevel_football;
    private String msLevel_vollyball;

    private String name, birthday,confirm;
    private String phone;
    private String date;
    private String email;
    private String msSex;
    private FirebaseDatabase FIREbaseDatebase;
    private DatabaseReference FIREBASEDatebase;
    private String password;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_member_information);

        mImageButton = (ImageButton) findViewById(R.id.imageButton2);
        mSpinner_sex = (Spinner) findViewById(R.id.spinner_Sex);
        mSpinner_basketball_level = (Spinner) findViewById(R.id.spinner_basketball_level);
        mSpinner_football_level = (Spinner) findViewById(R.id.spinner_football_level);
        mSpinner_vollyball_level = (Spinner) findViewById(R.id.spinner_vollyball_level);
        mCheckBox_Basketball = (CheckBox) findViewById(R.id.checkBox_Basketball);
        mCheckBox_Football = (CheckBox) findViewById(R.id.checkBox_Football);
        mCheckBox_Volleyball = (CheckBox) findViewById(R.id.checkBox_Volleyball);
        mConfirmRegister = (Button) findViewById(R.id.btn_finished_edit_info);
        mEditText_name = (EditText) findViewById(R.id.edtname);
        mEditText_phone = (EditText) findViewById(R.id.edtphone);
        mEditText_email = (EditText) findViewById(R.id.edtemail);
        mEditText_password = (TextInputLayout) findViewById(R.id.input_password);
        mEditText_confirm = (TextInputLayout) findViewById(R.id.confirm_pass);
        mConfirmRegister = (Button) findViewById(R.id.btn_finished_edit_info);
        mConfirmRegister.setOnClickListener(registerListener);

        mAuth = FirebaseAuth.getInstance();
        FIREbaseDatebase = FirebaseDatabase.getInstance();


        //建立Spinner_sex OnItemSelectedListener
        mSpinner_sex.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            //if Spinner_sex 有選取了任何值，跑下面程式碼
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                msSex = parent.getSelectedItem().toString();

                Toast.makeText(getApplicationContext(), "您選擇" + msSex, Toast.LENGTH_LONG).show();
            }

            @Override
            //else 沒有選取了任何值
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(), "您沒有選擇任何項目", Toast.LENGTH_LONG).show();
            }
        });


        //建立btn_GoCalendar觸發日曆
        theDate = (TextView) findViewById(R.id.date);
        mImageButton_cal = (ImageView) findViewById(R.id.imagebtn_GoCalendar);

        mImageButton_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        new_member_information.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListner,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

            }
        });

        mDateSetListner = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy" + month + "/" + day + "/" + year);

                date = month + "/" + day + "/" + year;
                theDate.setText(date);
            }
        };

        mCheckBox_Basketball.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCheckBox_Basketball.isChecked()) {
                    //建立Spinner_basketball_level OnItemSelectedListener
                    mSpinner_basketball_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_basketball_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_basketball = parent.getSelectedItem().toString();

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_basketball, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        //else 沒有選取了任何值
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } //Overwrite OnItemSelectedListener so that OnItemSelectedListener will not work when checkbox is already unchecked
                else {
                    //Reset position of Spinner to starting position
                    mSpinner_basketball_level.setSelection(0);
                    msLevel_basketball = null;
                    mSpinner_basketball_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_basketball_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_basketball = null;

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_basketball, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        });

        mCheckBox_Football.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCheckBox_Football.isChecked()) {
                    //建立Spinner_football_level OnItemSelectedListener
                    mSpinner_football_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_football_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_football = parent.getSelectedItem().toString();

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_football, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        //else 沒有選取了任何值
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    mSpinner_football_level.setSelection(0);
                    msLevel_football = null;
                    mSpinner_football_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_football_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_football = null;

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_football, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        //else 沒有選取了任何值
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        });

        mCheckBox_Volleyball.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCheckBox_Volleyball.isChecked()) {
                    //建立Spinner_vollyball_level OnItemSelectedListener
                    mSpinner_vollyball_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_vollyball_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_vollyball = parent.getSelectedItem().toString();

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_vollyball, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        //else 沒有選取了任何值
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    mSpinner_vollyball_level.setSelection(0);
                    msLevel_vollyball = null;
                    //建立Spinner_vollyball_level OnItemSelectedListener
                    mSpinner_vollyball_level.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        //if Spinner_vollyball_level 有選取了任何值，跑下面程式碼
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            msLevel_vollyball = null;

                            Toast.makeText(getApplicationContext(), "您選擇了等級: " + msLevel_vollyball, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        //else 沒有選取了任何值
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        });


        //當用家按下imagebutton後，顯示等級說明
        mImageButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "運動等級 1～5   1:最弱  5:最強", Toast.LENGTH_LONG).show();
            }
        });
    }

    //Get email & password toString() + Create new account + Display dialog if the signup is successful or not
    //Get name & phone
    private View.OnClickListener registerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            name = mEditText_name.getText().toString();
            phone = mEditText_phone.getText().toString();
            birthday = date;
            email = mEditText_email.getText().toString();
            password = mEditText_password.getEditText().getText().toString().trim();
            confirm = mEditText_confirm.getEditText().getText().toString().trim();

            //註冊前 檢查有否空值
            if (email.matches("") || birthday.matches("") ||
                    phone.matches("") || name.matches("") ||
                    password.matches("") ||confirm.matches("")) {

                Toast.makeText(getApplicationContext(), "請填寫所有欄位", Toast.LENGTH_LONG).show();

            }
            else if(!(password.equals(confirm))){
                Toast.makeText(getApplicationContext(), "密碼確認不符", Toast.LENGTH_LONG).show();
            }
            else if(password.equals(confirm)) {
                if (v.getId() == R.id.btn_finished_edit_info) {
                    email = mEditText_email.getText().toString().trim();
                    password = mEditText_password.getEditText().getText().toString().trim();
                    //連到firebase 建立新帳號
                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {

                                //成功
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        //Toast message indicate that registration is successful
                                        Toast.makeText(getApplicationContext(), "註冊成功!", Toast.LENGTH_LONG).show();

                                        //Get CurrentUser from Auth. server
                                        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

                                        //Setting path: User -> UID
                                        //currentFirebaseUser.getUid() <--- Can get UID of user's email
                                        FIREBASEDatebase = FIREbaseDatebase.getReference("User").child(currentFirebaseUser.getUid());


                                        //child -> key
                                        //path: Users -> UID -> Name
                                        //path: Users -> UID -> Sex
                                        //path: Users -> UID -> Phone
                                        //path: Users -> UID -> Email
                                        //path: Users -> UID -> Birthday
                                        //path: Users -> UID -> mBasketball_Level
                                        //path: Users -> UID -> mFootball_Level
                                        //path: Users -> UID -> mVolleyball_Level
                                        //Seems like path cannot use email address. Will cause app force close.
                                        FIREBASEDatebase.child("Name").setValue(name);
                                        FIREBASEDatebase.child("Sex").setValue(msSex);
                                        FIREBASEDatebase.child("Phone").setValue(phone);
                                        FIREBASEDatebase.child("Email").setValue(email);
                                        FIREBASEDatebase.child("Birthday").setValue(birthday);
                                        FIREBASEDatebase.child("Basketball_LV").setValue(msLevel_basketball);
                                        FIREBASEDatebase.child("Football_LV").setValue(msLevel_football);
                                        FIREBASEDatebase.child("Volleyball_LV").setValue(msLevel_vollyball);


                                        startActivity(new Intent(new_member_information.this, MainActivity.class));

                                    } //失敗
                                    else {
                                        new AlertDialog.Builder(new_member_information.this)
                                                .setMessage("註冊失敗")
                                                .setPositiveButton("確定", null)
                                                .show();
                                    }
                                }
                            });
                }
            }
            }


    };
}


