package com.example.a15;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class FindFriendActivity extends AppCompatActivity {
    private ImageButton SearchButton;
    private EditText SearchInputText;
    private RecyclerView SearchResultList;
    private DatabaseReference allUserDateBaseRef;
    private FirebaseRecyclerAdapter<FindFriends, FindFriendsViewHolder> adapter;
    private String searchBoxIuput;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friend);

        allUserDateBaseRef = FirebaseDatabase.getInstance().getReference().child("User");

        allUserDateBaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        SearchInputText = (EditText) findViewById(R.id.search_box_input);
        SearchButton = (ImageButton) findViewById(R.id.search_friends_button);

        SearchResultList = (RecyclerView) findViewById(R.id.search_result_list);
//        SearchResultList.setHasFixedSize(true);
        SearchResultList.setLayoutManager(new LinearLayoutManager(this));
//       SearchResultList.setAdapter(null);


        SearchButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                searchBoxIuput = SearchInputText.getText().toString();
                Toast.makeText(getApplicationContext(),"Searching....",Toast.LENGTH_LONG).show();
                SearchPeopleAndFriends(searchBoxIuput);
            }
        });
        //SearchPeopleAndFriends(searchBoxIuput);
    }



    private void SearchPeopleAndFriends(String searchBoxIuput)
    {/*
        Query UsersRef = allUserDateBaseRef.orderByChild("Name").startAt(searchBoxIuput).limitToLast(50);
        FirebaseRecyclerOptions<FindFriends> options=new FirebaseRecyclerOptions.Builder<FindFriends>().
                setQuery(UsersRef, FindFriends.class).build(); //query build past the query to FirebaseRecyclerAdapter
        FirebaseRecyclerAdapter<FindFriends, FindFriendsViewHolder> adapter=new FirebaseRecyclerAdapter<FindFriends, FindFriendsViewHolder>(options){

*/
        Query searchPeopleandFriendsQuery = allUserDateBaseRef.orderByChild("Name").startAt(searchBoxIuput).limitToLast(50);
        //Query searchPeopleandFriendsQuery = allUserDateBaseRef.orderByChild("Name").startAt(searchBoxIuput).endAt(searchBoxIuput).limitToLast(50);

        FirebaseRecyclerOptions<FindFriends> options = new FirebaseRecyclerOptions.Builder<FindFriends>().setQuery(searchPeopleandFriendsQuery, FindFriends.class).build();

        adapter = new FirebaseRecyclerAdapter<FindFriends, FindFriendsViewHolder>(options) {


            @NonNull
            @Override
            public FindFriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                FindFriendActivity.super.onStart();

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_users_display_layout, parent, false);
                //View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.all_users_display_layout, parent, false);

                return new FindFriendsViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull FindFriendsViewHolder viewHolder, final int i, @NonNull final FindFriends findFriends) {
                Log.d("onBindViewHolder:", "Checkpoint 1: ");

                final String Postkey = getRef(i).getKey();
                viewHolder.name.setText(findFriends.getName());
                viewHolder.status.setText(findFriends.getStatus());
                viewHolder.setProfileImage(getApplicationContext(), findFriends.getProfileImage());

                viewHolder.itemView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                        Intent intent = new Intent(FindFriendActivity.this, FindFriendActivity.class);
                        intent.putExtra("Postkey", Postkey);
                        startActivity(intent);

                        String visit_user_id = getRef(i).getKey();
                        Intent profileIntent = new Intent(FindFriendActivity.this,PersonProfileActivity.class);
                        profileIntent.putExtra("visit_user_id",visit_user_id);
                        startActivity(profileIntent);


                    }
                });
            }
        };
        adapter.startListening();
        SearchResultList.setAdapter(adapter);

    }

    public class FindFriendsViewHolder extends RecyclerView.ViewHolder {
        TextView name, status;

        public void setProfileImage(Context ctx,String profileImage){
            ImageView mimage = (ImageView)itemView.findViewById(R.id.imageIv);
            Picasso.get().load(profileImage).into(mimage);
        }

        public FindFriendsViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name_Tv);
            status = itemView.findViewById(R.id.statusTv);
        }


    }

    @Override
    protected void onStop() {
        if(adapter != null)
            adapter.stopListening();
        super.onStop();
    }

}