package com.example.a15;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class review_hardcoded extends AppCompatActivity {
    private DatabaseReference firebase_user_db;
    private TextView name1_textview, name2_textview;
    private ImageView profileImage1, profileImage2;
    private Button submit_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review_hardcoded);

        name1_textview = (TextView) findViewById(R.id.name_feedback_1);
        name2_textview = (TextView) findViewById(R.id.name_feedback_2);
        profileImage1 = (ImageView) findViewById(R.id.image_feedback_1);
        profileImage2 = (ImageView) findViewById(R.id.image_feedback_2);
        submit_btn = (Button) findViewById(R.id.submit_review_Btn);

        firebase_user_db = FirebaseDatabase.getInstance().getReference().child("User");

        submit_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "評分已送出!", Toast.LENGTH_LONG).show();
                finish();
            }
        });

        firebase_user_db.child("b5yoI9bJcZPSQwRs2sv0ektfb4m1").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                name1_textview.setText(dataSnapshot.child("Name").getValue().toString());
                if (dataSnapshot.child("ProfileImage").exists()) {
                    String string = dataSnapshot.child("ProfileImage").getValue(String.class);
                    Picasso.get().load(string).into(profileImage1);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        firebase_user_db.child("anztAzurG8UMEXMqJnG9Q488CUy1").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                name2_textview.setText(dataSnapshot.child("Name").getValue().toString());
                if (dataSnapshot.child("ProfileImage").exists()) {
                    String string = dataSnapshot.child("ProfileImage").getValue(String.class);
                    Picasso.get().load(string).into(profileImage2);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
