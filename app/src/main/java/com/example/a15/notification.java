package com.example.a15;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class notification extends AppCompatActivity {
    private Button mBtn_accept_invitation;
    private Button mBtn_reject_invitation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.notification);

        mBtn_accept_invitation = (Button) findViewById(R.id.btn_accept_invitation);
        mBtn_reject_invitation = (Button) findViewById(R.id.btn_reject_invitation);

        mBtn_accept_invitation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //邀約確認、寫入database （待寫）
            }
        });

        mBtn_reject_invitation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
