package com.example.a15;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.FirebaseRecyclerOptions.Builder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class check_member extends AppCompatActivity {

    private FirebaseRecyclerAdapter<FindInvitations, check_member.InvitationsViewHolder> adapter;
    private FirebaseAuth mAuth;
    private String online_user_id;
    private DatabaseReference InvitaionsRef;
    private DatabaseReference UsersRef;
    private RecyclerView recyclerview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.check_member);

        mAuth = FirebaseAuth.getInstance();
        online_user_id = mAuth.getCurrentUser().getUid();
        //InvitaionsRef = FirebaseDatabase.getInstance().getReference().child("Invitation").child(online_user_id);
        InvitaionsRef = FirebaseDatabase.getInstance().getReference().child("Invitation");
        UsersRef = FirebaseDatabase.getInstance().getReference().child("User");

        recyclerview = (RecyclerView)findViewById(R.id.checkmember_recyclerview);
        //myFriendList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setReverseLayout(true);
//        linearLayoutManager.setStackFromEnd(true);
        recyclerview.setLayoutManager(linearLayoutManager);

        DisplayAllInvitation();
    }
    private void DisplayAllInvitation() {
        //Query searchInvitationsQuery = InvitaionsRef.orderByChild("Name").startAt(searchBoxInput).limitToLast(50);

        //https://stackoverflow.com/questions/56441879/how-to-filter-data-from-firebase-database-realtime-with-complicated-nodes
        //Query searchInvitationsQuery = InvitaionsRef.child("26_8_2019").orderByChild("籃球/Date").limitToLast(50);
        //Query searchInvitationsQuery = InvitaionsRef.orderByChild("Date").limitToLast(50);
        //Query searchInvitationsQuery = InvitaionsRef.orderByChild("籃球").limitToLast(50);

        //FirebaseRecyclerOptions<FindInvitations> options = new Builder<FindInvitations>().setQuery(searchInvitationsQuery, FindInvitations.class).build();

        FirebaseRecyclerOptions<FindInvitations> options = new Builder<FindInvitations>().setQuery(InvitaionsRef, FindInvitations.class).build();
        adapter
                = new FirebaseRecyclerAdapter<FindInvitations, InvitationsViewHolder>(options) {


            @NonNull
            @Override
            public InvitationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                check_member.super.onStart();

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.check_member_recyclerview, parent, false);

                return new InvitationsViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull final InvitationsViewHolder invitationsViewHolder, int i, @NonNull FindInvitations findInvitations) {
                //Log.d("Output: ", FriendsRef.getKey());
                //Can get first unknown node
                final String node = getRef(i).getKey();
                Log.d("TAG:  ", node);
/*
                InvitaionsRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            //Iterable<DataSnapshot> a = dataSnapshot.child(node).getChildren();
                            //final String Place_name = dataSnapshot.child(online_user_id).child("籃球").child("Place_name").getRef().toString();
                            if (dataSnapshot.child(online_user_id).child("Place_name").child("籃球").exists()) {
                                final String Place_name = dataSnapshot.child(online_user_id).child("Place_name").child("籃球").getValue().toString();
                                Log.d("Path:  ", Place_name);
                                invitationsViewHolder.Sport.setText(Place_name);
                            }
                            final String Date = dataSnapshot.child("Date").getKey();
                            //final String Place_name = dataSnapshot.child("Place_name").getValue().toString();

                            invitationsViewHolder.Date.setText("  日期：" + node);

                            //invitationsViewHolder.Sport.setText(a.toString());
                            }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

 */


                invitationsViewHolder.Date.setText("日期：" + findInvitations.getDate());
                invitationsViewHolder.Sport.setText(findInvitations.getSport());
                invitationsViewHolder.Place_name.setText(findInvitations.getPlace_name());
                invitationsViewHolder.Start_time.setText(findInvitations.getStart_Time());
                invitationsViewHolder.Sport_Level.setText("LV: " + findInvitations.getSport_Level_Minimum() + "~" + findInvitations.getSport_Level_Maximum());

                invitationsViewHolder.layout.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Return to current RecyclerView when user pressed "back"
                        finish();
                        Intent intent = new Intent(check_member.this, check_member.class);
                        intent.putExtra("node", node);
                        startActivity(intent);

                        //連到manage_invitation 顯示被點擊邀約的 詳細資料   (Expected to let the user choose 答應 / 取消邀約）
                        Intent intent2 = new Intent(getApplicationContext(), manage_invitation.class);
                        intent2.putExtra("node", node);
                        startActivity(intent2);
                        //setContentView(R.layout.manage_invitation);

                    }
                });
/*
                //!!!!  get position of the recyclerview
                //final int position = invitationsViewHolder.getAdapterPosition();
                invitationsViewHolder.lyt_parent.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        invitationsViewHolder.lyt_parent.setBackgroundColor(Color.parseColor("#85b8CB"));
                        //Toast.makeText(getApplicationContext(), "The position is: " + position, Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "UID:   " + userIDs, Toast.LENGTH_SHORT).show();
                        //UID_selected.add(userIDs);
                        //count++;
                    }
                });

 */
            }
        };
        adapter.startListening();
        recyclerview.setAdapter(adapter);

    }

    private class InvitationsViewHolder extends RecyclerView.ViewHolder{
        TextView Date, Sport, Place_name, Start_time, Sport_Level;
        ConstraintLayout layout;


        public InvitationsViewHolder(@NonNull View itemView) {
            super(itemView);
            Date = itemView.findViewById(R.id.textView_IV_date);
            Sport = itemView.findViewById(R.id.textView_IV_sport);
            Place_name = itemView.findViewById(R.id.textView_IV_location);
            Start_time = itemView.findViewById(R.id.textView_IV_start_time);
            Sport_Level = itemView.findViewById(R.id.textView_IV_sportLV);
            layout = itemView.findViewById(R.id.layout_check_member);

        }
/*
        public void setLyt_parent_color_transparent() {
            lyt_parent.setBackgroundColor(Color.TRANSPARENT);
        }

 */

    }

    @Override
    protected void onStop() {
        if(adapter != null)
            adapter.stopListening();
        super.onStop();
    }
}
