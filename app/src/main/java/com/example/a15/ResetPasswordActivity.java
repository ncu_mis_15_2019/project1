package com.example.a15;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ResetPasswordActivity extends AppCompatActivity {
    private Button ResetPasswordSendEmailButton;
    private EditText ResetEmailInput;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        mAuth = FirebaseAuth.getInstance();
        ResetPasswordSendEmailButton = (Button)findViewById(R.id.reset_password_send);
        ResetEmailInput = (EditText)findViewById(R.id.reset_password_Email);

        ResetPasswordSendEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userEmail = ResetEmailInput.getText().toString();

                if(TextUtils.isEmpty(userEmail)){
                    Toast.makeText(ResetPasswordActivity.this,"Please write your valid email address first...",Toast.LENGTH_SHORT).show();
                }
                else
                    {
                        mAuth.sendPasswordResetEmail(userEmail).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful())
                                {
                                    Toast.makeText(ResetPasswordActivity.this,"Please check your Email Account, If you want to reset your password...",Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(ResetPasswordActivity.this, login.class));
                                }
                                else
                                    {
                                        String message = task.getException().getMessage();
                                        Toast.makeText(ResetPasswordActivity.this, "Error Occurred: "+ message,Toast.LENGTH_SHORT).show();
                                    }
                            }
                        });
                    }
            }
        });
    }
}
