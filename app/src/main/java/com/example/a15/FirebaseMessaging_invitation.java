package com.example.a15;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;

import com.example.a15.chat_notification.AboveNotification;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FirebaseMessaging_invitation extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage){
        super.onMessageReceived(remoteMessage);

        //get current user from share preferences
        SharedPreferences sp = getSharedPreferences("SP_USER", MODE_PRIVATE);
        String savedCurrentUser = sp.getString("Current_USERID","None");

        String sent = remoteMessage.getData().get("sent");
        String user = remoteMessage.getData().get("user");
        FirebaseUser fUser = FirebaseAuth.getInstance().getCurrentUser();
        if (fUser != null && sent.equals(fUser.getUid())){
            if (!savedCurrentUser.equals(user)){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    sendAboveNotification(remoteMessage);
                }
                else{
                    sendNormalNotification(remoteMessage);
                }
            }
        }
    }


    private void sendNormalNotification(RemoteMessage remoteMessage) {
        String user = remoteMessage.getData().get("user");
        String icon_main = remoteMessage.getData().get("icon");
        String title_main = remoteMessage.getData().get("title");
        String body_main = remoteMessage.getData().get("body");

        RemoteMessage.Notification notification = remoteMessage.getNotification();
        int i = Integer.parseInt(user.replaceAll("[\\D]",""));
        Intent intent = new Intent(this, invitation_step4.class);
        Bundle bundle = new Bundle();
        bundle.putString("UIDs",user);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pIntent_main = PendingIntent.getActivity(this, i, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        //建立NotificationCompat.Builder物件
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, null)
                .setSmallIcon(Integer.parseInt(icon_main))
                .setContentText(body_main)
                .setContentTitle(title_main)
                .setAutoCancel(true)
                .setSound(defSoundUri)
                .setContentIntent(pIntent_main);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        int j =0;
        if (i>0){
            j = i;
        }
        notificationManager.notify(j, builder.build());
    }

    private void sendAboveNotification(RemoteMessage remoteMessage) {
        String user = remoteMessage.getData().get("user");
        String icon_main = remoteMessage.getData().get("icon");
        String title_main = remoteMessage.getData().get("title");
        String body_main = remoteMessage.getData().get("body");

        RemoteMessage.Notification notification = remoteMessage.getNotification();
        int i = Integer.parseInt(user.replaceAll("[\\D]",""));
        Intent intent = new Intent(this, invitation_step4.class);
        Bundle bundle = new Bundle();
        bundle.putString("UIDs",user);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pIntent_main = PendingIntent.getActivity(this, i, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        AboveNotification notification1 = new AboveNotification(this);

        Notification.Builder builder = notification1.getNotifications(title_main, body_main, defSoundUri, icon_main);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        int j =0;
        if (i>0){
            j = i;
        }
        notificationManager.notify(j, builder.build());
    }
}
