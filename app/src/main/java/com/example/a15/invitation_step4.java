package com.example.a15;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.a15.chat_notification.APIService;
import com.example.a15.chat_notification.Client;
import com.example.a15.chat_notification.Data;
import com.example.a15.chat_notification.ModelUser;
import com.example.a15.chat_notification.Response;
import com.example.a15.chat_notification.Sender;
import com.example.a15.chat_notification.Token;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class invitation_step4 extends AppCompatActivity {
    private Button mBtn_confirm_invitation;
    private TextView theDate;
    private ImageView mImageButton_cal;
    private OnDateSetListener mDateSetListner;
    private String date;
    private ImageView mImageButton_Sta_time;
    private ImageView mImageButton_End_time;
    private String start_time =null;
    private String end_time = null;
    private TextView mstart_time_textview;
    private TextView mend_time_textview;
    private String UID;
    private DatabaseReference FIREBASEDatebase;
    private String sex_restriction = null;
    private String age_restriction = null;
    private String sportLV_start = null;
    private String sportLV_end = null;
    private int num_of_ppl;
    private DatabaseReference FIREBASEDATABASE;
    private String date_return = null;
    private int count;
    private String sport;
    private List<String> UIDs;

    APIService apiService;
    boolean notify = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.invitation_step4);

        //create api service
        apiService = Client.getRetrofit("https://fcm.googleapis.com/").create(APIService.class);

        //update token
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String deviceToken = instanceIdResult.getToken();
                updateToken(deviceToken);
            }
        });

        num_of_ppl = Integer.parseInt(invitation_step1.getSelected_num_of_ppl());

        //判斷是好友 / 隨機邀約
        if (invitation_step1.getActivity_choose() == 0){
            UIDs = invitation_step2.getUID_selected();
        }else {
            UIDs = Random_SD_Invitation.getUID_selected();
        }

        Log.d("Step4", String.valueOf(invitation_step1.getActivity_choose()));
        for (int k = 0; k < UIDs.size(); k++) {
            Log.d("Step4", UIDs.get(k));
        }
/*
        final String p_name = MapsActivity.getPlace_name();
        final double p_Latitude = MapsActivity.getLatitude_final();
        final double p_Longitude = MapsActivity.getLongtude_final();
        final String ppl = invitation_step1.getSelected_num_of_ppl();

 */
        if (invitation_step1.get_SelectedSport().matches("籃球")) {
            sport = "籃\n球";
        }else if (invitation_step1.get_SelectedSport().matches("足球")){
            sport = "足\n球";
        }else {
            sport = "排\n球";
        }


        UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        //Database Reference
        FIREBASEDatebase = FirebaseDatabase.getInstance().getReference().child("Invitation");


        mBtn_confirm_invitation = (Button) findViewById(R.id.btn_confirm_invitation);

        mBtn_confirm_invitation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                notify = true;
                //送出邀約
                String invitation = "時間：" + start_time + "     日期：" + date + "     球類：" + sport +"                 地點: " + MapsActivity.getPlace_name();
                sendInvitation(invitation);

                String node = date_return + "_" + UID;
                FIREBASEDATABASE = FIREBASEDatebase.child(node);
                FIREBASEDATABASE.child("Sport").setValue(sport);
                FIREBASEDATABASE.child("Sport_horizontal").setValue(invitation_step1.get_SelectedSport());
                FIREBASEDATABASE.child("Start_Time").setValue(start_time);
                FIREBASEDATABASE.child("End_Time").setValue(end_time);
                FIREBASEDATABASE.child("Sex_Restriction").setValue(sex_restriction);
                FIREBASEDATABASE.child("Age_Restriction").setValue(age_restriction);
                FIREBASEDATABASE.child("Sport_Level_Minimum").setValue(sportLV_start);
                FIREBASEDATABASE.child("Sport_Level_Maximum").setValue(sportLV_end);
                FIREBASEDATABASE.child("Date").setValue(date_return);
                FIREBASEDATABASE.child("Number_of_people").setValue(invitation_step1.getSelected_num_of_ppl());
                FIREBASEDATABASE.child("Place_name").setValue(MapsActivity.getPlace_name());
                FIREBASEDATABASE.child("Place_Latitude").setValue(MapsActivity.getLatitude_final());
                FIREBASEDATABASE.child("Place_Longitude").setValue(MapsActivity.getLongtude_final());
                FIREBASEDATABASE.child("Members_Pending").setValue(UIDs);
                FIREBASEDATABASE.child("Initiator").setValue(UID);

/*
                if (date_return.matches("") || end_time.matches("") || start_time.matches("")){
                    Toast.makeText(getApplicationContext(),"請填寫所有欄位",Toast.LENGTH_LONG).show();
                }else {

 */
/*                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Old data structure !!!!!!!!!!!!!!!!!!!!!!!!!!
                    //把邀約條件寫進Database
                    FIREBASEDATABASE = FIREBASEDatebase.child(date_return).child(UID).child(invitation_step1.get_SelectedSport());
                    FIREBASEDATABASE.child("Start_Time").setValue(start_time);
                    FIREBASEDATABASE.child("End_Time").setValue(end_time);
                    FIREBASEDATABASE.child("Sex_Restriction").setValue(sex_restriction);
                    FIREBASEDATABASE.child("Age_Restriction").setValue(age_restriction);
                    FIREBASEDATABASE.child("Sport_Level_Minimum").setValue(sportLV_start);
                    FIREBASEDATABASE.child("Sport_Level_Maximum").setValue(sportLV_end);
                    FIREBASEDATABASE.child("Date").setValue(date_return);
                    FIREBASEDATABASE.child("Number_of_people").setValue(ppl);
                    FIREBASEDATABASE.child("Place_name").setValue(p_name);
                    FIREBASEDATABASE.child("Place_Latitude").setValue(p_Latitude);
                    FIREBASEDATABASE.child("Place_Longitude").setValue(p_Longitude);
                    FIREBASEDATABASE.child("Members").setValue(UIDs);

 */
/*
                Log.d("TAG", UIDs.get(1));
                Log.d("TAG", UIDs.get(0));

                Log.d("Num of ppl VALUE", String.valueOf(num_of_ppl));



                while (num_of_ppl > 0){
                    num_of_ppl--;
                    Log.d("Num of ppl VALUE", String.valueOf(num_of_ppl));
                    count = 0;
                    FIREBASEDATABASE.child("Members").child(String.valueOf(count)).setValue(UIDs.get(num_of_ppl));
                    count++;
                }

                do{
                    num_of_ppl--;
                    int count = 0;
                    FIREBASEDATABASE.child("Members").child(String.valueOf(count)).setValue(UIDs.get(num_of_ppl));
                    count++;
                }while (num_of_ppl > 0);
 */


                    //Toast message indicate that button being pressed
                    Toast.makeText(getApplicationContext(), "邀約已發出!", Toast.LENGTH_LONG).show();

                    //完成邀約，回到主畫面
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
//                }
            }
        });

        // Get spinner value
        Spinner mSex_restriction = (Spinner) findViewById(R.id.spinner_Sex_restriction);

        mSex_restriction.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sex_restriction = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Spinner mAge_restriction = (Spinner) findViewById(R.id.spinner_Age_restriction);

        mAge_restriction.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                age_restriction = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Spinner mSportLV_start = (Spinner) findViewById(R.id.spinner_SportLV_start);

        mSportLV_start.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sportLV_start = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Spinner mSportLV_end = (Spinner) findViewById(R.id.spinner_SportLV_end);

        mSportLV_end.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sportLV_end = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });




            //建立btn_GoCalendar觸發日曆
        theDate = (TextView) findViewById(R.id.textView_date);
        mImageButton_cal = (ImageView) findViewById(R.id.imagebtn_Calendar);

        mImageButton_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        invitation_step4.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListner,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

            }
        });

        mDateSetListner = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d("TAG", "onDateSet: mm/dd/yyy" + month + "/" + day + "/" + year);

                date = day + "/" + month + "/" + year;
                date_return = day + " " + month + " " + year;
                theDate.setText(date);
            }
        };

        //建立日期觸發事件
        mImageButton_Sta_time = (ImageView) findViewById(R.id.imagebtn_Starting_Time);
        mImageButton_End_time = (ImageView) findViewById(R.id.imagebtn_End_Time);
        mstart_time_textview = (TextView) findViewById(R.id.textView_start_time);
        mend_time_textview = (TextView) findViewById(R.id.textView_end_time);

        mImageButton_Sta_time.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Use the current time as the default values for the picker
                final Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);
                // Create a new instance of TimePickerDialog and return it
                new TimePickerDialog(invitation_step4.this, new TimePickerDialog.OnTimeSetListener(){

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        start_time = hourOfDay + ":" + minute;
                        mstart_time_textview.setText(start_time);
                    }
                }, hour, minute, false).show();
            }

        });

        mImageButton_End_time.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Use the current time as the default values for the picker
                final Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);
                // Create a new instance of TimePickerDialog and return it
                new TimePickerDialog(invitation_step4.this, new TimePickerDialog.OnTimeSetListener(){

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        end_time = hourOfDay + ":" + minute;
                        mend_time_textview.setText(end_time);
                    }
                }, hour, minute, false).show();
            }

        });
    }

    //送出邀約
    private void sendInvitation(final String invitation) {

        if (notify){
            for (int k = 0; k < UIDs.size(); k++)
                sendNotification(UIDs.get(0), invitation);
        }else {
            notify = false;
        }

    }

    //送出通知
    private void sendNotification(final String UIDs, final String invitation) {
        DatabaseReference allTokens = FirebaseDatabase.getInstance().getReference("Tokens");
        Query query = allTokens.orderByKey().equalTo(UIDs);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds: dataSnapshot.getChildren()){
                    Token token = ds.getValue(Token.class);
                    Data data = new Data(UID, invitation,"邀約通知",UIDs,R.drawable.logo);

                    Sender sender = new Sender(data, token.getToken());
                    apiService.sendNotification(sender)
                            .enqueue(new Callback<Response>() {
                                @Override
                                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                    Toast.makeText(invitation_step4.this,""+response.message(),Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onFailure(Call<Response> call, Throwable t) {

                                }
                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //update token
    private void updateToken(String token){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Tokens");
        Token mToken = new Token(token);
        ref.child(UID).setValue(mToken);
    }
}
