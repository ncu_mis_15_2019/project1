package com.example.a15;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PersonProfileActivity extends AppCompatActivity {

    private TextView userName, userGender, userBirthday, userBasketballLV, userFootballLV, userVolleyballLV, userRelation;
    private ImageView userProfileImage;
    private Button Send_fd_request, Reject_fd_request;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference FriendRequestRef, UsersRef, FriendsRef;
    private String senderUserId, receicerUserId, CURRENT_STATE, saveCurrentDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_profile);

        firebaseAuth = FirebaseAuth.getInstance();
        senderUserId = firebaseAuth.getCurrentUser().getUid();

        receicerUserId = getIntent().getExtras().get("visit_user_id").toString();
        UsersRef = FirebaseDatabase.getInstance().getReference().child("User");
        FriendRequestRef = FirebaseDatabase.getInstance().getReference().child("FriendRequests");
        FriendsRef = FirebaseDatabase.getInstance().getReference().child("Friends");

        InitializeFields();

        UsersRef.child(receicerUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            private String UserName, UserSex, UserBirthday, UserBasketball,UserVolleyball, UserFootball, UserProfileImage, UserRelation;

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    if (dataSnapshot.child("Name").exists()) {
                        UserName = dataSnapshot.child("Name").getValue().toString();
                    }
                    if (dataSnapshot.child("Sex").exists()) {
                        UserSex = dataSnapshot.child("Sex").getValue().toString();
                    }
                    if (dataSnapshot.child("Birthday").exists()) {
                        UserBirthday = dataSnapshot.child("Birthday").getValue().toString();
                    }
                    if (dataSnapshot.child("Basketball_LV").exists()) {
                        UserBasketball = dataSnapshot.child("Basketball_LV").getValue().toString();
                    }
                    if (dataSnapshot.child("Volleyball_LV").exists()) {
                        UserVolleyball = dataSnapshot.child("Volleyball_LV").getValue().toString();
                    }
                    if (dataSnapshot.child("Football_LV").exists()) {
                        UserFootball = dataSnapshot.child("Football_LV").getValue().toString();
                    }
                    if (dataSnapshot.child("ProfileImage").exists()) {
                        UserProfileImage = dataSnapshot.child("ProfileImage").getValue().toString();
                    }
                    if (!CURRENT_STATE.matches("")) {
                        UserRelation = CURRENT_STATE;
                    }
                    /*
                    if (dataSnapshot.child("Relationship").exists()) {
                        UserRelation = dataSnapshot.child("Relationship").getValue().toString();
                    }
                    */

                    if (UserProfileImage != null) {
                        Picasso.get().load(UserProfileImage).into(userProfileImage);
                    }

                    userName.setText("姓名: "+UserName);
                    userGender.setText("性別: "+UserSex);
                    userBirthday.setText("生日: "+UserBirthday);
                    userBasketballLV.setText("籃球等級: "+UserBasketball);
                    userVolleyballLV.setText("排球等級: "+UserVolleyball);
                    userFootballLV.setText("足球等級: "+UserFootball);
                    //userRelation.setText("好友關係: "+UserRelation);


                    MaintenanceofButtons();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Reject_fd_request.setVisibility(View.INVISIBLE);
        Reject_fd_request.setEnabled(false);

        //避免可加自己帳號好友
        if (!senderUserId.equals(receicerUserId))
        {
            Send_fd_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Send_fd_request.setEnabled(false);

                    //申請成為好友
                    if (CURRENT_STATE.equals("not_friends")) {
                        SendFriendRequestToaPerson();
                    }
                    //取消好友申請
                    if (CURRENT_STATE.equals("request_sent")) {
                        CancelFriendRequest();
                    }
                    //接受成為好友
                    if (CURRENT_STATE.equals("request_received")){
                        AcceptFriendrequest();
                    }
                    //取消好友關係
                    if (CURRENT_STATE.equals("friends")){
                        UnFriendAnExistingFriend();
                    }
                }
            });
        }
        else
            {
                Reject_fd_request.setVisibility(View.INVISIBLE);
                Send_fd_request.setVisibility(View.INVISIBLE);
            }

    }

    //取消好友關係
    private void UnFriendAnExistingFriend() {
        FriendsRef.child(senderUserId).child(receicerUserId)
                .removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful())
                        {
                            FriendsRef.child(receicerUserId).child(senderUserId)
                                    .removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if(task.isSuccessful())
                                            {
                                                Send_fd_request.setEnabled(true);
                                                CURRENT_STATE = "not_friends";
                                                Send_fd_request.setText("發送好友邀請");
                                                userRelation.setText("好友關係: "+CURRENT_STATE);

                                                Reject_fd_request.setVisibility(View.INVISIBLE);
                                                Reject_fd_request.setEnabled(false);
                                            }
                                        }
                                    });

                        }
                    }
                });
    }

    //FriendRequests & Friends 在Firebase內會互相影響
    //接受成為好友
    private void AcceptFriendrequest() {
        Calendar calDorDate = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("dd-MMM-yyyy");
        saveCurrentDate = currentDate.format(calDorDate.getTime());

        FriendsRef.child(senderUserId).child(receicerUserId).child("date").setValue(saveCurrentDate)  //紀錄成為好友時間
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            FriendsRef.child(receicerUserId).child(senderUserId).child("date").setValue(saveCurrentDate)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                FriendRequestRef.child(senderUserId).child(receicerUserId)
                                                        .removeValue()
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if(task.isSuccessful())
                                                                {
                                                                    FriendRequestRef.child(receicerUserId).child(senderUserId)
                                                                            .removeValue()
                                                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                @Override
                                                                                public void onComplete(@NonNull Task<Void> task)
                                                                                {
                                                                                    if(task.isSuccessful())  //成為好友
                                                                                    {
                                                                                        Send_fd_request.setEnabled(true);
                                                                                        CURRENT_STATE = "friends";
                                                                                        Send_fd_request.setText("解除好友關係");
                                                                                        userRelation.setText("好友關係: "+CURRENT_STATE);

                                                                                        Reject_fd_request.setVisibility(View.INVISIBLE);
                                                                                        Reject_fd_request.setEnabled(false);
                                                                                    }
                                                                                }
                                                                            });

                                                                }
                                                            }
                                                        });
                                            }
                                        }
                                    });
                        }
                    }
                });
    }





    //取消好友申請
    private void CancelFriendRequest()
    {
        FriendRequestRef.child(senderUserId).child(receicerUserId)
                .removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful())
                        {
                            FriendRequestRef.child(receicerUserId).child(senderUserId)
                                    .removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if(task.isSuccessful())
                                            {
                                                Send_fd_request.setEnabled(true);
                                                CURRENT_STATE = "not_friends";
                                                Send_fd_request.setText("發送好友邀請");
                                                userRelation.setText("好友關係: "+CURRENT_STATE);

                                                Reject_fd_request.setVisibility(View.INVISIBLE);
                                                Reject_fd_request.setEnabled(false);
                                            }
                                        }
                                    });

                        }
                    }
                });
    }

    //維持button的request狀態
    private void MaintenanceofButtons()
    {
        FriendRequestRef.child(senderUserId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(receicerUserId))
                        {
                            String request_type = dataSnapshot.child(receicerUserId).child("request_type").getValue().toString();

                            if (request_type.equals("sent"))
                            {
                                CURRENT_STATE = "request_sent";
                                Send_fd_request.setText("取消好友邀請");
                                userRelation.setText("好友關係: "+CURRENT_STATE);

                                Reject_fd_request.setVisibility(View.INVISIBLE);
                                Reject_fd_request.setEnabled(false);
                            }
                            else if (request_type.equals("received"))
                            {
                                CURRENT_STATE = "request_received";
                                Send_fd_request.setText("接受好友邀請");
                                userRelation.setText("好友關係: "+CURRENT_STATE);

                                Reject_fd_request.setVisibility(View.VISIBLE);
                                Reject_fd_request.setEnabled(true);

                                Reject_fd_request.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        CancelFriendRequest();
                                    }
                                });
                            }
                        }
                        else
                            {
                                FriendsRef.child(senderUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.hasChild(receicerUserId)){
                                            CURRENT_STATE ="friends";
                                            Send_fd_request.setText("解除好友關係");
                                            userRelation.setText("好友關係: "+CURRENT_STATE);

                                            Reject_fd_request.setVisibility(View.INVISIBLE);
                                            Reject_fd_request.setEnabled(false);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    //發送好友請求，新增FriendRequests在Firebase
    private void SendFriendRequestToaPerson()
    {
        FriendRequestRef.child(senderUserId).child(receicerUserId)
                .child("request_type").setValue("sent")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful())
                        {
                            FriendRequestRef.child(receicerUserId).child(senderUserId)
                                    .child("request_type").setValue("received")
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if(task.isSuccessful())
                                            {
                                                Send_fd_request.setEnabled(true);
                                                CURRENT_STATE = "request_sent";
                                                Send_fd_request.setText("取消好友邀請");
                                                userRelation.setText("好友關係: "+CURRENT_STATE);

                                                Reject_fd_request.setVisibility(View.INVISIBLE);
                                                Reject_fd_request.setEnabled(false);
                                            }
                                        }
                                    });

                        }
                    }
                });

    }

    private void InitializeFields() {
        userName = (TextView) findViewById(R.id.person_username);
        userGender = (TextView) findViewById(R.id.person_gender);
        userBirthday = (TextView) findViewById(R.id.person_dob);
        userBasketballLV = (TextView) findViewById(R.id.person_basketball);
        userFootballLV = (TextView) findViewById(R.id.person_football);
        userVolleyballLV = (TextView) findViewById(R.id.person_volleyball);
        userRelation = (TextView) findViewById(R.id.person_relationship_status);
        userProfileImage = (ImageView) findViewById(R.id.person_profile_pic);

        Send_fd_request = (Button) findViewById(R.id.person_send_friend_request_btn);
        Reject_fd_request = (Button) findViewById(R.id.person_decline_friend_request_btn);

        CURRENT_STATE = "not_friends";
    }
}
