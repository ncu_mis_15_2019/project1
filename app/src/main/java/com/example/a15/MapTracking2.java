package com.example.a15;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import android.location.Location;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;

public class MapTracking2 extends FragmentActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    private String email;
    private DatabaseReference locations;
    private static Double lat, lng;
    private static double fdlat, fdlng;
    private String selected_user_id;
    private DatabaseReference db;
    private String selected_user_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_tracking);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Ref to firebase first
        locations = FirebaseDatabase.getInstance().getReference("Locations");

        //Get Intent
        if (getIntent() != null)
        {
            email = getIntent().getStringExtra("Email");
            selected_user_id = getIntent().getStringExtra("UserID");
            lat = ListOnline2.getCurr_lat_int();
            lng = ListOnline2.getCurr_lng_int();

            // Selected user's location
            locations.child(selected_user_id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //Get Lat & Lng values (selected user)
                    String lat_temp = dataSnapshot.child("latitude").getValue().toString();
                    String lng_temp = dataSnapshot.child("longitude").getValue().toString();

                    fdlat = Double.parseDouble(lat_temp);
                    fdlng = Double.parseDouble(lng_temp);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            db = FirebaseDatabase.getInstance().getReference().child("User");
            db.child(selected_user_id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //Get selected_user Email
                    selected_user_email = dataSnapshot.child("Email").getValue().toString();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            loadLocationForThisUser();
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setZoomControlsEnabled(true); //顯示縮放圖示

        mMap.getUiSettings().setRotateGesturesEnabled(true); //可用手勢控制地圖旋轉
        mMap.getUiSettings().setScrollGesturesEnabled(true); //可用手勢控制地圖左右移動
        mMap.getUiSettings().setZoomGesturesEnabled(true); //可使用手勢控制地圖縮放


    }

    private void loadLocationForThisUser() {
        Query user_location = locations;
        user_location.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Clear all old markers
                mMap.clear();

                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng),8));

                //Create marker for current user
                LatLng current = new LatLng(lat, lng);
                mMap.addMarker(new MarkerOptions().position(current).title(FirebaseAuth.getInstance().getCurrentUser().getEmail())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));


                //Create marker for selected user
                LatLng selected_user = new LatLng(fdlat, fdlng);
                mMap.addMarker(new MarkerOptions().position(selected_user).title(selected_user_email)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                        .snippet("Distance: (KM)" + distance()/1000));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private double distance() {
        float pk = (float) (180.f/Math.PI);

        double a1 = lat / pk;
        double a2 = lng / pk;
        double b1 = fdlat / pk;
        double b2 = fdlng / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        //Return distance in --->  Meters <-----
        return 6366000 * tt;
    }
}
